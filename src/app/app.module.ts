import { NgModule, NO_ERRORS_SCHEMA  } from "@angular/core";
import { ExtraOptions, RouterModule, Routes } from '@angular/router';

import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from "./app.component";
import { IntroComponent } from "./intro/intro.component";
import { QrFormComponent } from "./qr-form/qr-form.component";

const routes: Routes = [
  {
    path: '',
    component: IntroComponent,
  },
  {
    path: 'generate',
    component: QrFormComponent
  },
  { path: '**', redirectTo: '' },
];

const config: ExtraOptions = {
  useHash: false,
  onSameUrlNavigation: 'reload',
};

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes, config),
    NgbModule,
  ],
  declarations: [
    AppComponent,
    IntroComponent,
    QrFormComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
