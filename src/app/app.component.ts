import { Component } from '@angular/core';

@Component({
  selector: 'vcq-app',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
})
export class AppComponent  {
}
