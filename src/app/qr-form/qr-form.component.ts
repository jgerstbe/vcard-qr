import { Component, TemplateRef, ViewChild } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import * as QRCode from 'qrcode';
import * as vCardsJS from '../../assets/vcard-js';
import * as download from 'downloadjs';

@Component({
  selector: 'qr-form',
  templateUrl: './qr-form.component.html',
  styleUrls: ['./qr-form.component.css']
})
export class QrFormComponent  {
  @ViewChild('qrModal', {static: false}) qrModal: TemplateRef<any>;
  @ViewChild('alertModal', {static: false}) alertModal: TemplateRef<any>;
  closeResult: string;
  error: any = {
    title: 'Fehler!',
    message: 'Es ist ein Fehler aufgetreten.'
  }
  // vcf
  vCard: any = vCardsJS();
  qr:any;
  email: string = "";

  constructor(private modalService: NgbModal) {    
    this.vCard.workAddress.label = 'Firmenanschrift';
    this.vCard.version = '3.0';
  }

  generateQr() {
    if (!this.vCard || !this.validateVcard()) {
      this.displayError('Mindestens die Fehler "Vorname", "Nachname" und "E-Mail" müssen für die Erstellung eines VCard-QR ausgefüllt sein.');
      return;
    }
    // open modal
    this.open(this.qrModal);
    QRCode.toCanvas(document.getElementById('qrCanvas'), this.vCard.getFormattedString(), function (error) {
      if (error) console.error(error)
    });
  }
  
  validateVcard():boolean {
    let valid: boolean = true;
    const required = ['firstName', 'lastName', 'workEmail'];
    for (let r of required) {
      valid = valid && this.vCard[r] ? this.vCard[r].length > 0 : false;
    }
    return valid;
  }

  async download(type:string) {
    if (!this.vCard) {
      this.displayError('Es konnten keine VCard-Daten gefunden werden.');
      return;
    }
    if (type === 'SVG') {
      QRCode.toString(this.vCard.getFormattedString(), {type: 'svg'}, (error, string) => {  
        if (error) {
          console.error(error);
          return;
        }      
        download(string, 'vcard-qr.svg', 'image/svg+xm');
      });
    } else if (type === 'PNG') {
      // download(document.getElementById('qrCanvas').toDataURL(), 'vcard-qr.png', 'image/png');
      QRCode.toDataURL(this.vCard.getFormattedString(), (error, string) => {  
        if (error) {
          console.error(error);
          return;
        }      
        download(string, 'vcard-qr.png', 'image/png');
      });
    } else if (type === 'vcf') {
      download(this.vCard.getFormattedString(), 'vcard.vcf', 'text/vcard');
    }
  }

  open(content) {
    console.log('open', content)
    const modal = this.modalService.open(content, { centered: true });
    modal.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    return modal;
  }

  displayError(message: string = 'Es ist ein Fehler aufgetreten.', title: string = 'Fehler!') {
    this.error = {
      message: message,
      title: title
    }
    this.open(this.alertModal);
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
